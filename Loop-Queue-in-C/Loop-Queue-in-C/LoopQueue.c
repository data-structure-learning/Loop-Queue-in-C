//
//  LoopQueue.c
//  Loop-Queue-in-C
//
//  Created by 买明 on 20/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//
#include <stdlib.h>
#include "LoopQueue.h"

void InitQueue(P_LoopQueue q, int queueCapacity) {
    q->m_pQueue = (int *)malloc(sizeof(int)*queueCapacity);
    
    if (q->m_pQueue == NULL) {
        exit(-1);
    }
    q->m_iQueueCapacity = queueCapacity;
    ClearQueue(q);
}

void FreeQueue(P_LoopQueue q) {
    free(q->m_pQueue);
    q->m_pQueue = NULL;
}

void ClearQueue(P_LoopQueue q) {
    q->m_iHead = 0;
    q->m_iTail = 0;
    q->m_iQueueLen = 0;
}

int QueueEmpty(P_LoopQueue q) {
    return q->m_iQueueLen == 0;
}

int QueueFull(P_LoopQueue q) {
    return q->m_iQueueLen == q->m_iQueueCapacity;
}

int QueueLength(P_LoopQueue q) {
    return q->m_iQueueLen;
}

int EnQueue(P_LoopQueue q, int element) {
    if (QueueFull(q)) {
        return 0;
    }
    
    q->m_pQueue[q->m_iTail] = element;
    q->m_iTail += 1;
    q->m_iTail %= q->m_iQueueCapacity;
    q->m_iQueueLen += 1;
    return 1;
}

int DeQueue(P_LoopQueue q, int *element) {
    if (QueueEmpty(q)) {
        return 0;
    }
    
    *element = q->m_pQueue[q->m_iHead];
    q->m_iHead += 1;
    q->m_iHead %= q->m_iQueueCapacity;
    q->m_iQueueLen -= 1;
    return 1;
}

void QueueTraverse(P_LoopQueue q) {
    for (int i = q->m_iHead; i < q->m_iHead + q->m_iQueueLen; i ++) {
        printf("%d ", q->m_pQueue[i % q->m_iQueueLen]);
    }
    printf("\n");
}
