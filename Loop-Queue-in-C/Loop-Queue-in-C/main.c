//
//  main.c
//  Loop-Queue-in-C
//
//  Created by 买明 on 20/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#include <stdio.h>
#include "LoopQueue.h"

void testLoopQueue();

int main(int argc, const char * argv[]) {
    
    printf("testLoopQueue\n");
    testLoopQueue();
    
    return 0;
}

void testLoopQueue() {
    LoopQueue q;
    
    InitQueue(&q, 5);
    
    EnQueue(&q, 2);
    EnQueue(&q, 5);
    
    int e = 0;
    DeQueue(&q, &e);
    printf("e: %d\n", e);
    
    EnQueue(&q, 7);
    EnQueue(&q, 12);
    EnQueue(&q, 19);
    EnQueue(&q, 31);
    EnQueue(&q, 50);
    
    QueueTraverse(&q);
    
    DeQueue(&q, &e);
    DeQueue(&q, &e);
    DeQueue(&q, &e);
    DeQueue(&q, &e);
    DeQueue(&q, &e);
    DeQueue(&q, &e);
    
    QueueTraverse(&q);
    
    EnQueue(&q, 81);
    ClearQueue(&q);
    
    QueueTraverse(&q);
    
    FreeQueue(&q);
}
