//
//  LoopQueue.h
//  Loop-Queue-in-C
//
//  Created by 买明 on 20/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef LoopQueue_h
#define LoopQueue_h

#include <stdio.h>

typedef struct {
    
    int *m_pQueue;
    int m_iQueueLen;
    int m_iQueueCapacity;
    
    int m_iHead;
    int m_iTail;
    
} LoopQueue, *P_LoopQueue;

void InitQueue(P_LoopQueue q, int queueCapacity);
void FreeQueue(P_LoopQueue q);
void ClearQueue(P_LoopQueue q);
int QueueEmpty(P_LoopQueue q);
int QueueFull(P_LoopQueue q);
int QueueLength(P_LoopQueue q);
int EnQueue(P_LoopQueue q, int element);
int DeQueue(P_LoopQueue q, int *element);
void QueueTraverse(P_LoopQueue q);

#endif /* LoopQueue_h */
